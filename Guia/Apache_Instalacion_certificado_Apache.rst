Instalación Certificado Apache Server
=====================================

Para instalar su Certificado SSL en Apache por favor, siga estas instrucciones.

Nos conectamos via SSH al servidor ::

	# ssh <user>@<Ip Server>

cuando nos encontremos en el servidor ir a la ruta de instalación del Apache Server, si hacemos la instalacion desde los repositorios de Centos el mismo quedara instalado en  **/etc/httpd/** ::

	# cd /etc/httpd/

en esta carpeta se encuentran dos rutas las cuales usaremos **conf** y **conf.d** ::

	[root@lcsdevapptdcms httpd]# ls -ltr
	total 0
	lrwxrwxrwx 1 root root  10 May 18 12:38 run -> /run/httpd
	lrwxrwxrwx 1 root root  29 May 18 12:38 modules -> ../../usr/lib64/httpd/modules
	lrwxrwxrwx 1 root root  19 May 18 12:38 logs -> ../../var/log/httpd
	drwxr-xr-x 2 root root 184 May 18 12:38 conf.modules.d
	drwxr-xr-x 2 root root  60 May 19 10:30 conf
	drwxr-xr-x 2 root root 220 May 19 10:33 conf.d

en **conf.d** encontraremos un archivo llamado Aplicacion-ssl.conf donde se encuentra la configuración SSL del virtual Server, en el cual encontramos la ruta donde se realizo la instalación los certificados::

	SSLCertificateFile /etc/pki/tls/directus/cert.pem
	SSLCertificateKeyFile /etc/pki/tls/directus/private.key
	SSLCertificateChainFile /etc/pki/tls/directus/ca.crt

Luego nos vamos al archivo ssl.conf en el cual se encuentran las rutas de un archivo el cual tiene la frase secreta SSLPassPhraseDialog ::

	SSLPassPhraseDialog exec:/etc/httpd/conf/passphrase.conf

Luego de tener esto configurado verificamos la configuración de apache para estar seguros de que nada falle al reiniciar el aplicativo::

	# apachectl configtest

si nada falla reiniciamos el apache ::

	# sytemctl restart httpd
