X-FRAME-OPTIONS
=============================

Para defenderse del ataque Clickjacking en su servidor web Apache, puede utilizar 
X-FRAME-OPTIONS para evitar que su sitio web sea pirateado por Clickjacking.

Esto evitará que el contenido del sitio se incruste en otros sitios.

Hay tres configuraciones para X-Frame-Options:

SAMEORIGIN : esta configuración permitirá que una página se muestre en un marco en el mismo origen que la página en sí.
DENY : Esta configuración evitará que una página se muestre en un marco o iframe.
PERMITIR DESDE uri : esta configuración permitirá que una página se muestre solo en el origen especificado.

Implementar en Apache, IBM HTTP Server
--------------------------------------

Login to Apache or IHS server

Take a backup of a configuration file

Add following line in httpd.conf file

Header always append X-Frame-Options SAMEORIGIN

Restart the respective webserver to test the application

Implementar en alojamiento web compartido
----------------------------------------------------

 adding the following line in the .htaccess file.

 Header append X-FRAME-OPTIONS "SAMEORIGIN"

 Change is reflected immediately without doing any restart.


